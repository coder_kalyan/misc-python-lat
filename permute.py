# regular way
def permute(l,a=""):
    if l == []:
        print(a)
    else:
        for i in l:
            l2 = l[:]
            l2.remove(i)
            permute(l2,a+i)

# for fun, a little more concise, a lot less readable
def permute2(l,a=""):
    if l == []:
        print(a)
    else:
        [permute([x for x in l if x != i],a+i) for i in l]

def permute3(l,a=""):
    if l == []:
        print(a)
    else:
        for j in l:
            for i in j:
                l2 = l[:]
                l2.remove(i)
                permute(l2,a+i)


permute3([['A','B','C'],['D','E','F'],['G','H','I'],['J','K','L']])

