while True:
    n = int(input("Number of students:"))
    start = 1
    multiplier = 1
    while n > 1:
        multiplier *= 2
        if n % 2 == 0:
            n //= 2
        else:
            n //= 2
            start += multiplier
    print(start)
